# GitHub Stars Rest API

[Task](https://bitbucket.org/aleksandrnb/coding-challenge-backend/src/master/CHALLENGE-TASK.md): 
Create a simple RESTful API that imports data from GitHub Search API https://developer.github.com/v3/search/ 

### Functionality:
* Get the list of all repositories
* Sort repositories by any field name 
    * `sort=[field_name]` - ascending order
    * `sort=-[field_name]` - descending order
* Set pagination parameters
     * `per_page=[number]` - how many items to display
     * `page=[number]` - page number to open

`http://127.0.0.1:8000/repositories/?page=4&per_page=8&sort=stargazers_count` - link example

### How to run
```
$ git clone https://aleksandrnb@bitbucket.org/aleksandrnb/coding-challenge-backend.git

$ cd coding-challenge-backend
```
Create a virtualenv to isolate our package dependencies locally
```
$ virtualenv env

$ source env/bin/activate  # On Windows use `env\Scripts\activate
```
Install requirements
```
$ pip install -r requirements.txt

```
Run script to fill the db
```
$ python load_data.py
```
Run the server
```
$ python manage.py runserver
```
