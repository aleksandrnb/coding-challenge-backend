from django.conf.urls import url, include
from app import views
from rest_framework import routers

router = routers.SimpleRouter()
router.register(r'repositories', views.RepositoriesViewSet, base_name='setting_values')

urlpatterns = [
   url(r'^', include(router.urls)),
]