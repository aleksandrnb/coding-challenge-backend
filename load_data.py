from pymongo import MongoClient
import time
import requests
import json
import os

"""script for for filling mongodb"""

client = MongoClient('localhost', 27017)
db = client.api

FIELDS = ('full_name', 'html_url', 'description', 'stargazers_count', 'language')
GITHUB_URL = 'https://api.github.com/search/repositories?q=language:python+stars:>{stars_amount}&page={page}&per_page=100'


def filter_by_fields(item):
    return {field: item[field] for field in item if field in FIELDS}

i = 10
res = []
print('loading data...')
while i > 0:
    url_req = GITHUB_URL.format(stars_amount=500, page=i)
    raw_data = requests.get(url_req)
    if raw_data.status_code == 403:
        # needed to avoid requests limitations
        time.sleep(100)
    else:
        json_data = json.loads(raw_data.text)
        items = json_data.get('items')
        res += [filter_by_fields(item) for item in items]
        i -= 1

db.repositories.insert_many(res)
print('loading is finished...')

