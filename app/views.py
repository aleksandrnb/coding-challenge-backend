from rest_framework_mongoengine import viewsets
from app.serializers import RepositoriesSerializer
from app.models import Repositories

"""main view to display repositories"""


class RepositoriesViewSet(viewsets.ModelViewSet):
    serializer_class = RepositoriesSerializer

    def get_queryset(self):
        sort_param = self.request.GET.get('sort')
        if sort_param:
            return Repositories.objects.order_by(sort_param)
        else:
            return Repositories.objects.all()
