from mongoengine import Document, fields

"""db model description"""

class Repositories(Document):
    full_name = fields.StringField()
    description = fields.StringField()
    language = fields.StringField()
    html_url = fields.StringField()
    stargazers_count = fields.IntField()
