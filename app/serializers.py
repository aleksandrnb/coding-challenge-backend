from rest_framework_mongoengine import serializers as mongoserializers
from rest_framework import serializers
from app.models import Repositories


class RepositoriesSerializer(mongoserializers.DocumentSerializer):
    class Meta:
        model = Repositories
        fields = ['full_name', 'html_url', 'description', 'stargazers_count', 'language']
