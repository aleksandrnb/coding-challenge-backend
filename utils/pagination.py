from rest_framework.pagination import PageNumberPagination

"""pagination description"""

class PaginationParam(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'per_page'
